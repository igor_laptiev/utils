﻿using IniParser;
using IniParser.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace FileSystemInspectorRunner
{
    public class Configuration
    {
        private const int DefaultIntervalMs = 1000;
        private string _folder;
        private bool _restartRequired;
        private string _filter;
        private string _copySource;
        private string _copyTarget;
        private string _start;
        private int _timeout;
        private readonly string _fileName;
        private DirectoryMonitor _cfgWatcher;

        ILog _logger = LogManager.GetLogger("Configuration");

        public Boolean Debug
        {
            get => _debug;
            set
            {
                _debug = value;
                ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).Root.Level = _debug?log4net.Core.Level.Debug: log4net.Core.Level.Info;
                ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).RaiseConfigurationChanged(EventArgs.Empty);
            }
        }

        public string Folder
        {
            get => _folder;
            set
            {
                if (_folder != value)
                {
                    _folder = value;
                    RestartRequired = true;
                }
            }
        }

        public string Filter
        {
            get => _filter;
            set
            {
                if (_filter != value)
                {
                    _filter = value;
                    RestartRequired = true;
                }
            }
        }

        public int Timeout
        {
            get => _timeout;
            private set
            {
                if (_timeout != value)
                {
                    _timeout = value;
                    RestartRequired = true;
                }

            }
        }
        public string CopySource
        {
            get => _copySource;
            set
            {
                if (_copySource != value)
                {
                    _copySource = value;
                }
            }
        }
        public string CopyTarget
        {
            get => _copyTarget;
            set
            {
                if (_copyTarget != value)
                {
                    _copyTarget = value;
                }
            }
        }

        public string StartProgram
        {
            get => _start;
            set
            {
                if (_start != value)
                {
                    _start = value;
                }
            }
        }

        private bool _debug;

        public bool RestartRequired { get => _restartRequired; set => _restartRequired = value; }
        public string StartParameters { get; internal set; }

        public event Action OnChanged;

        public Configuration(string configurationName)
        {
            _fileName = GetFileName(configurationName);
            if (string.IsNullOrEmpty(_fileName))
            {
                var errorString = string.Format("Wrong config file or not found: {0}", configurationName);
                MessageBox.Show(errorString, "Stop", MessageBoxButtons.OK);
                throw (new Exception(errorString));
            }

            Configure();
            //RestartRequired = false;
            Watch();
        }

        private void Watch()
        {
            _cfgWatcher = new DirectoryMonitor(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Path.GetFileName(_fileName));
            _cfgWatcher.CollectTimeout = DefaultIntervalMs;
            _cfgWatcher.Changed += (evnt) => 
            {
                _logger.InfoFormat("Config changed, {0}: {1}", _restartRequired ? "restarting" : "", _fileName);
                Configure();
                OnChanged?.Invoke();
                RestartRequired = false;
            };
            _cfgWatcher.Start();
        }

        public void Configure()
        {
            var parser = new FileIniDataParser();
            IniData cfg = parser.ReadFile(_fileName);
            bool __debug;
            Debug = Boolean.TryParse(cfg["GeneralConfiguration"]["Debug"], out __debug)? __debug: false;
            Folder = Environment.ExpandEnvironmentVariables(cfg["Watch"]["Folder"]??string.Empty);
            Filter = cfg["Watch"]["FileName"];
            int __timeout;
            Timeout = Int32.TryParse(cfg["Watch"]["Timeout"], out __timeout) ? __timeout : DefaultIntervalMs;
            CopySource = Environment.ExpandEnvironmentVariables(cfg["Action"]["CopySource"] ?? string.Empty);
            CopyTarget = Environment.ExpandEnvironmentVariables(cfg["Action"]["CopyTarget"] ?? string.Empty);
            StartProgram = Environment.ExpandEnvironmentVariables(cfg["Action"]["Start"] ?? string.Empty);
            StartParameters = Environment.ExpandEnvironmentVariables(cfg["Action"]["StartParameters"] ?? string.Empty);
        }

        private string GetFileName(string configurationName)
        {
            return CheckFile(configurationName)
                ?? CheckFile(Path.ChangeExtension(configurationName, "ini"))
                ?? CheckFile(Path.ChangeExtension(configurationName, "cfg"))
                ?? CheckFile(Path.ChangeExtension(configurationName, "config"));
        }

        private string CheckFile(string name)
        {
            if (File.Exists(name))
            {
                return name;
            }

            return null;
        }
    }
}
