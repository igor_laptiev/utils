﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace FileSystemInspectorRunner
{
    public delegate void FileSystemEvent(String path);

    public class DirectoryMonitor
    {
        private readonly FileSystemWatcher _fileSystemWatcher =  new FileSystemWatcher();
        private readonly Dictionary<string, DateTime> _pendingEvents = new Dictionary<string, DateTime>();
        private readonly Timer _timer;
        private bool _timerStarted = false;

        public int CollectTimeout { get; internal set; }

        public DirectoryMonitor(string path , string filter)
        {
            _fileSystemWatcher.Path = path;
            _fileSystemWatcher.Filter = filter;
            _fileSystemWatcher.IncludeSubdirectories = false;
            _fileSystemWatcher.Created += OnChange;
            _fileSystemWatcher.Changed += OnChange;

            _timer = new Timer(OnTimeout, null, Timeout.Infinite, Timeout.Infinite);
        }

        public event FileSystemEvent Changed;

        public void Start()
        {
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void OnChange(object sender, FileSystemEventArgs e)
        {
            // Don't want other threads messing with the pending events right now
            lock (_pendingEvents)
            {
                // Save a timestamp for the most recent event for this path
                _pendingEvents[e.FullPath] = DateTime.Now;

                log4net.LogManager.GetLogger("DirMon").Debug(string.Format(" - FSW OnChange, {0}, {1}", e.FullPath, e.ChangeType));
                // Start a timer if not already started
                if (!_timerStarted)
                {
                    _timer.Change(CollectTimeout, CollectTimeout);
                    _timerStarted = true;
                }
            }
        }

        private void OnTimeout(object state)
        {
            List<string> paths;
            log4net.LogManager.GetLogger("DirMon").Debug(string.Format(" - timeout"));

            // Don't want other threads messing with the pending events right now
            lock (_pendingEvents)
            {
                // Get a list of all paths that should have events thrown
                paths = FindReadyPaths(_pendingEvents);

                // Remove paths that are going to be used now
                paths.ForEach((path) =>
                {
                    _pendingEvents.Remove(path);
                });

                // Stop the timer if there are no more events pending
                if (_pendingEvents.Count == 0)
                {
                    _timer.Change(Timeout.Infinite, Timeout.Infinite);
                    _timerStarted = false;
                }
            }

            // Fire an event for each path that has changed
            paths.ForEach((path) =>
            {
                log4net.LogManager.GetLogger("DirMon").Debug(string.Format(" - fire, {0}", path));
                FireEvent(path);
            });
        }

        private List<string> FindReadyPaths(Dictionary<string, DateTime> events)
        {
            DateTime now = DateTime.Now;

            return events.Where(entry => now.Subtract(entry.Value).TotalMilliseconds >= CollectTimeout)
                .Select(entry => entry.Key)
                .ToList();
        }

        private void FireEvent(string path)
        {
            Changed?.Invoke(path);
        }
    }
}
