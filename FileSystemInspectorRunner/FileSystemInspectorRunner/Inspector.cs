﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;

namespace FileSystemInspectorRunner
{
    public class Inspector
    {
        ILog _logger = LogManager.GetLogger("Inspector");
        private DirectoryMonitor _mainWatcher;
        private Configuration _configuration;

        public Inspector(string configurationName)
        {
            _configuration = new Configuration(configurationName);
            _configuration.OnChanged += Config_Changed;
        }

        private void StartWatch()
        {
            if (!_configuration.RestartRequired) return;

            _logger.InfoFormat("Watching: {0}, {1}", _configuration.Folder, _configuration.Filter);
            if (_mainWatcher!=null)
            {
                _mainWatcher.Changed -= File_Changed;
                _mainWatcher = null;
            }

            _mainWatcher = new DirectoryMonitor(_configuration.Folder, _configuration.Filter);
            _mainWatcher.CollectTimeout = _configuration.Timeout;
            _mainWatcher.Changed += File_Changed;
            _mainWatcher.Start();
        }

        private void File_Changed(string path)
        {
            _logger.InfoFormat("File changed: {0}", path);
            CopyAndRun();
        }

        private void CopyAndRun()
        {
            if (string.IsNullOrEmpty(_configuration.CopySource))
            {
                _logger.Warn("The CopySource setting is empty. Nothing to do.");
                return;
            }
            if (string.IsNullOrEmpty(_configuration.CopyTarget))
            {
                _logger.Warn("The CopyTarget setting is empty. Nothing to do.");
                return;
            }
            if (!File.Exists(_configuration.CopySource) || IsDirectory(_configuration.CopySource))
            {
                _logger.WarnFormat("The copy source {0} doesn't exist or isn't a file.", _configuration.CopySource);
                return;
            }

            Directory.CreateDirectory(Path.GetDirectoryName(_configuration.CopyTarget));
            File.Copy(_configuration.CopySource, _configuration.CopyTarget, true);
            _logger.InfoFormat("File copied from {0} to {1}", _configuration.CopySource, _configuration.CopyTarget);

            if (!File.Exists(_configuration.StartProgram))
            {
                _logger.WarnFormat("The file to start doesn't exist: {0}.", _configuration.StartProgram);
                return;
            }
            _logger.InfoFormat("Starting {0}", _configuration.StartProgram);
            Process.Start(new ProcessStartInfo
            {
                FileName = _configuration.StartProgram,
                Arguments = _configuration.StartParameters,
                WorkingDirectory = Path.GetDirectoryName(_configuration.StartProgram)
            });
        }

        private Boolean IsDirectory(string path)
        {
            FileAttributes attr = File.GetAttributes(path);

            return (attr & FileAttributes.Directory) == FileAttributes.Directory;
        }

        private void Config_Changed()
        {
            StartWatch();
        }

        public void Start()
        {
            StartWatch();
        }
    }
}
