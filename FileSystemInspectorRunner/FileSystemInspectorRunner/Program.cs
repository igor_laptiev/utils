﻿using FileSystemInspectorRunner.Properties;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace FileSystemInspectorRunner
{
    static class Program
    {
        static ILog _logger = LogManager.GetLogger("Main");
        static NotifyIcon _ni = new NotifyIcon();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            ApplicationInit();
            LogInit();
            if (args.Length == 0)
            {
                MessageBox.Show("Restart the programm with a parameter: fsi.exe <configName>. Press OK to exit.", "Stop", MessageBoxButtons.OK);
                throw new Exception("Restart the programm with a parameter: fsi.exe <configName>");
            }
            _logger.Info(string.Format("Started with config: {0}", args[0]));

            StartInTray(args[0]);
        }

        private static void StartInTray(string config)
        {
            _ni.Visible = true;
            _ni.Text = "File System Inspector";
            _ni.Icon = new Icon(Resources.tray, 40, 40);
            _ni.ContextMenuStrip = new ContextMenuStrip();
            _ni.ContextMenuStrip.Items.AddRange(new ToolStripItem[]
            {
                new ToolStripMenuItem("Выйти из FSI",null,(sender, args) =>
                {
                    _ni.Visible = false;
                    _logger.Info("User exits ...");
                    Application.Exit();
                }),
            });
            _ni.Visible = true;
            var inspector = new Inspector(config);
            inspector.Start();
            Application.Run();
        }

        private static void ApplicationInit()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new ThreadExceptionEventHandler(ThreadExceptionHandler);

            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }

        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            LogAndExit(e.Exception, !(e.Exception is ApplicationException));
        }

        private static void LogAndExit(Exception exception, Boolean exit = false)
        {
            if (_logger != null)
            {
                _logger.Error(exception);
                if (exit)
                {
                    _logger.Info("Exitting...");
                }
                else
                {
                    _logger.Info("Continuing...");
                }
            }
            else
            {
                string errorMsg = @"An application error occurred. Please contact the adminstrator 
                                       with the following information:\n\n";
                errorMsg = errorMsg + exception.Message + "\n\nStack Trace:\n" + exception.StackTrace;
                MessageBox.Show(errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (exit)
            {
                _ni.Visible = false;
                System.Environment.Exit(1);
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogAndExit((Exception)e.ExceptionObject, !(e.ExceptionObject is ApplicationException));
        }

        private static void LogInit()
        {

            var hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.Level = log4net.Core.Level.Info;
            hierarchy.Root.RemoveAllAppenders(); /*Remove any other appenders*/
            FileAppender __fileAppender = new FileAppender
            {
                AppendToFile = true,
                LockingModel = new FileAppender.MinimalLock(),
                File = "fsi.log"
            };
            PatternLayout __pl = new PatternLayout { ConversionPattern = "%d [%2%t] %-5p [%-10c]   %m%n" };
            __pl.ActivateOptions();
            __fileAppender.Layout = __pl;
            __fileAppender.ActivateOptions();
            BasicConfigurator.Configure(__fileAppender);

/*            var config = new LoggingConfiguration();

            var consoleTarget = new ColoredConsoleTarget("console")
            {
                Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}"
            };

            config.AddTarget(consoleTarget);
            var fileTarget = new FileTarget("filetarget")
            {
                FileName = "${basedir}/fsi.log",
                Layout = "${longdate} ${level} ${message}  ${exception}",
                AutoFlush = true,
                ConcurrentWrites = true,
                DeleteOldFileOnStartup = true
            };
            config.AddTarget(fileTarget);
            config.AddRuleForOneLevel(LogLevel.Error, fileTarget);
            config.AddRuleForOneLevel(LogLevel.Warn, fileTarget);
            config.AddRuleForOneLevel(LogLevel.Info, fileTarget);
            config.AddRuleForAllLevels(consoleTarget);

            LogManager.Configuration = config;*/
        }
    }
}
